const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    res.status(405).send ({"error" : "method not allowed"})
})

router.post('/', (req, res, next) => {
    const {social, cnpj, endereco, num_endereco, compl_endereco, cep, cidade, UF, telefone, web, email} = req.body
    if (!social){
        res.status(400).send("Preciso do seu nome")
    }

    if (!email){
        res.status(400).send({"error" : "email required"})
    }
    else{
        if(!email.includes("@") && email.includes(".")){
            res.status(400).send({"error" : "not valid email"})
        }
        res.send(req.body)
    }
    
    if (!web){
        res.status(400).send({"error" : "URL required"})
    }
    else{
        if(!web.includes("http:\\" || "https:\\") && web.includes(".")){
            res.status(400).send({"error" : "not valid URL"})
        }
        res.send(req.body)
    }

    if (!telefone){
        res.status(400).send("Eu quero seu telefone")
    }
    
    if (!endereco){
        res.status(400).send("Eu quero seu endereço")
    }

    res.send(req.body)
})

module.exports = router;
