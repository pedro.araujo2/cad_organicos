const express = require('express')
let bodyParser = require ('body-parser')
const app = express()
const PORT = 3001

app.use(express.json())
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

app.use('/', require('./routes/api/cadastro'))
app.use('/cadastro', require('./routes/api/cadastro'))

app.listen(PORT, () => console.log(`Conectado à http://localhost:${PORT}`))
